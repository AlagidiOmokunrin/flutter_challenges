import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:rect_getter/rect_getter.dart';

import 'baritemmodel.dart';

class ChallengeBottomNavigationBar extends StatefulWidget {
  @override
  _ChallengeBottomNavigationBarState createState() =>
      _ChallengeBottomNavigationBarState();
}

class _ChallengeBottomNavigationBarState
    extends State<ChallengeBottomNavigationBar>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  static const double MAX_LIFT_SIZE = 2;

  int selectedIndex = 0;
  int previousIndex = 0;

  @override
  void initState() {
    _controller =
        AnimationController(duration: Duration(milliseconds: 500), vsync: this);
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);

    super.initState();
  }

  void _afterLayout(_) {
    onNavTap(0);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var barItems = <BottomNavigationBarItem>[];
    for (int i = 0; i < barModels.length; i++) {
      var barModel = barModels[i];
      barItems.add(new BottomNavigationBarItem(
          icon: AnimatedBuilder(
              animation: _controller,
              builder: (BuildContext context, Widget child) {
                return RectGetter(
                  key: barModel.iconPosKey,
                  child: Container(
                    height: 24,
                    width: 24,
                    child: FlareActor(
                      barModel.fileName,
                      fit: BoxFit.cover,
                      animation: animationName(i),
                    ),
                  ),
                );
              }),
          title: barModel.textWidget));
    }
    return Stack(
      children: [
        new BottomNavigationBar(
          backgroundColor: const Color(0xFF040407),
          type: BottomNavigationBarType.fixed,
          unselectedItemColor: Colors.grey,
          selectedItemColor: const Color(0xFF6367A2),
          currentIndex: selectedIndex,
          items: barItems,
          onTap: onNavTap,
        ),
        AnimatedPositioned(
          curve: Curves.easeInOut,
          duration: const Duration(milliseconds: 250),
          bottom: 10,
          left: barModels[selectedIndex].activeIconPosX,
          child: Container(
              height: 10,
              width: 10,
              decoration: BoxDecoration(
                  color: const Color(0xFF079AEC), shape: BoxShape.circle)),
        )
      ],
    );
  }

  void onNavTap(int newIndex) {
    setState(() {
      previousIndex = selectedIndex;
      selectedIndex = newIndex;
      for (int i = 0; i < barModels.length; i++) {
        var barModel = barModels[i];
        barModel.onBarIndexChange(i == selectedIndex);
        _controller.reset();
        _controller.forward();
      }
    });
  }

  String animationName(i) {
    if (selectedIndex == i) return "Select";
    if (previousIndex == i)
      return "DeSelect";
    else
      return "idle";
  }
}

final barModels = <TabBarItemModel>[
  TabBarItemModel("Home"),
  TabBarItemModel("Camera"),
  TabBarItemModel("Files"),
  TabBarItemModel("Profile"),
];
